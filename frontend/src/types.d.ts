export interface ButtonProps {
  title: string,
  className: string,
  onClick: () => void,
}

export interface HeaderProps {
  isLogged: boolean;
}

export interface IPatient {
  id: number,
  first_name: string,
  last_name: string,
  email: string
}

export interface IConsultationsResponse {
  id: number,
  patientId: number,
  date: Date,
  patient: IPatient
}

export interface ILoginResponse {
  name: string,
  token: string
}

export interface Login {
  email: string,
  password: string
}

export interface NewConsultation {
  patientId: string | number,
  date: Date
}

export interface FooterProps {
  pacients: IPatient[];
}
