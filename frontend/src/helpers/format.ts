export const formatDate = (date: Date) => {
  let newDate = new Date(date);
  let year = newDate.getFullYear();
  let month = newDate.getMonth() + 1; //getMonth is zero based;
  let day = newDate.getDate();
  let dayString = day < 10 ? `0${day}` : day
  let time = newDate.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
  let stringMonth = month < 10 ? `0${month}` : month;

  let formatted = `${dayString}/${stringMonth}/${year} ás ${time}`;

  return formatted;
}

export const formatTypeDate = (date: string, time: string) => {
  var hours = time.split(":")[0];
  var minutes = time.split(":")[1];
  const dt = new Date(date)

  let year = dt.getFullYear();
  let month = dt.getMonth(); //getMonth is zero based;
  let day = dt.getDate() + 1;
  dt.setDate(day);
  dt.setMonth(month);
  dt.setFullYear(year);
  dt.setHours(parseInt(hours));
  dt.setMinutes(parseInt(minutes));

  return dt;
}
