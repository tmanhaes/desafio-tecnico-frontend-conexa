import React, { FormEvent, useEffect, useState } from 'react';
import ActionButton from '../../components/ActionButton';
import { Container, Form, ActionsBox } from './style';
import Modal ,{  } from 'react-modal';
import { FooterProps, NewConsultation } from '../../types';
import { newConsultation } from '../../services';
import { formatTypeDate } from '../../helpers/format';
import { useHistory } from 'react-router-dom';

const customStyles = {
  content : {
    top           : '50%',
    left          : '50%',
    right         : 'auto',
    bottom        : 'auto',
    marginRight   : '-50%',
    transform     : 'translate(-50%, -50%)',
    width         : '100%',
    maxWidth      : '400px'
  }
};

const Footer: React.FC<FooterProps> = ({ pacients }) => {
  const history = useHistory();
  const [openModal, setOpenModal] = useState(false);
  const [pacientId, setPacientId] = useState(0);
  const [consultDate, setConsultDate] = useState('');
  const [time, setTime] = useState('');

  const helpOnClick = () => { alert("Help click"); }

  const scheduleClick = () => { setOpenModal(true); }

  const closeModal = () => { 
    setOpenModal(false); 
    history.push('/consultas');
  }

  const scheduleConsult = async (event: FormEvent) => {
    event.preventDefault();
    if(consultDate && time && pacientId){
      const dt = formatTypeDate(consultDate, time);
      try {
        const response = await newConsultation(pacientId, dt.toString());
        if(response){
          alert('consulta agendada com sucesso!');
          closeModal();
          history.push('/consultas');
          window.location.reload(); 
        }
      }catch(e){
        alert(`erro ao agendar consulta. Erro: ${e}`);
      }
    }
  }

  return (
    <>
      <Container>
        <ActionButton 
          title="Ajuda" 
          className="helpButton" 
          onClick={helpOnClick}
        />
        <ActionButton 
          title="Agendar consulta" 
          className="scheduleButton" 
          onClick={scheduleClick}
        />
      </Container>
      {
        openModal === true ? (
          <Modal
            isOpen={openModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel="Nova Consulta"
          >
            <Form onSubmit={scheduleConsult}>
              <h3>Nova Consulta</h3>
              <label>
                <span>Paciente: </span>
                <select onChange={e => setPacientId(parseInt(e.target.value))} required>
                  <option value="<------ selecione ------->" >Selecione</option>
                  {
                    pacients.map(pacient => {
                      return (
                        <option 
                          key={pacient.id} 
                          value={pacient.id}
                        >
                          {pacient.first_name + ' ' + pacient.last_name}
                        </option>
                      )
                    })
                  }
                </select>
              </label>
              <label>
                <span>Data: </span>
                <input 
                  required
                  type="date" 
                  value={consultDate} 
                  onChange={e => setConsultDate(e.target.value)}
                />
              </label>

              <label>
                <span>Hora: </span>
                <input 
                  required
                  type="time" 
                  value={time} 
                  onChange={e => setTime(e.target.value)}
                />
              </label>
              
              <ActionsBox>
                <ActionButton 
                  title="Agendar" 
                  className="schedule" 
                  onClick={() => undefined}
                />

                <ActionButton 
                  title="cancelar" 
                  className="cancel" 
                  onClick={closeModal}
                />
              </ActionsBox>
            </Form>
          </Modal>
        ) : (
          <></>
        )
      }
    </>
  );
}

export default Footer;
