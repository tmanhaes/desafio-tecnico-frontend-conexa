import styled from 'styled-components';

export const Container = styled.footer`
  padding: 10px 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const Form = styled.form`
  width: 100%;
  height: 400px;

  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  @media(max-width: 768px){
    width: 90%;
    margin: 0 auto;
  }

  > .helpButton {
    width: 100%;
    height: 30px;
  }

  > h3 {
    ont-family: var(--font-Monserrat);
    font-size: var(--font-size-large);
    font-weight: var(--font-weight-bold);
    color: var(--dark-blue);
    margin: 10px 0 ;
  }

  > label {
    width: 100%;
    margin: 5px 0;
    > span {
      font-family: var(--font-Nunito);
      font-weight: var(--font-weight-normal);
      font-size: var(--font-size-normal);
      color: var(--dark-gray);
      margin: 5px 0;;
    }

    > select {
      margin: 5px 0;
      width: calc(100% - 10px);
      height: 30px;
    }

    > input {
      margin: 5px 0;
      height: 30px;
      width: calc(100% - 10px);
    }
  }
`;

export const ActionsBox = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-direction: row;

  .schedule {
    margin-right: 10px;
    > span {
      color: var(--light-blue);
    }
    &:hover {
      background: var(--light-blue);
  
      > span {
        color: #fff;
      }
    }
  }

  .cancel {
    background: red;
    border: 2px solid red;
    > span {
      color: #fff;
    }
  }

`;
