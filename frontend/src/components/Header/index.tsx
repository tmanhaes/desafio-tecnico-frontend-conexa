import React from 'react';
import Logo from '../../assets/logo-conexa.svg';
import { useHistory } from 'react-router-dom';
import { Container, Image, DrInfo, ButtonLogOut } from './style';
import { HeaderProps } from '../../types';

const LogOutButton = () => {
  const history = useHistory();

  const logOut = () => {
    localStorage.setItem('token', '');
    localStorage.setItem('name', '');
    localStorage.setItem('isLogged', '');
    history.push('/');
  } 

  return (
    <ButtonLogOut onClick={logOut}>
      <span>Sair</span>
    </ButtonLogOut>
  )
}

const Header: React.FC<HeaderProps> = ({ isLogged }) => {
  const drName = localStorage.getItem('name');

  return (
    <Container>
      <Image>
        <img src={Logo} alt="logo"/>
      </Image>  
      {
        isLogged  ? (
          <DrInfo>
            <span>Olá, Dr. {drName}</span>
            <LogOutButton />
          </DrInfo>
        ) : (
          <></>
        )
      }
    </Container>
  );
}

export default Header;
