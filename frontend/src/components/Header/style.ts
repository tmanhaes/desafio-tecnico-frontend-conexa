import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;  
  padding: 15px 20px;
  box-shadow: 0px 12px 20px 1px rgba(0,0,0,.1);
  font-family: var(--font-Nunito);
`;

export const Image = styled.div``;

export const DrInfo = styled.div`
  > span {
    margin-right: 10px;
    color: var(--dark-gray);
    font-weight: var(--font-weight-medium);

    @media(max-width: 768px){
      display: none;
    }
  }
`;

export const ButtonLogOut = styled.button`
  background: transparent;
  border: 2px solid var(--light-blue);
  border-radius: 6px;
  padding: 5px 15px;
  cursor: pointer;
  transition: all 350ms ease;

  > span {
    color: var(--light-blue);
    font-weight: var(--font-weight-bold);
  }

  &:hover {
    background: var(--light-blue);

    > span {
      color: #fff;
    }
  }
`;
