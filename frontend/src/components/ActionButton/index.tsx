import React from 'react';
import { ButtonProps } from '../../types';
import { Button } from './style';

const ActionButton: React.FC<ButtonProps> = ({ title, className, onClick }) => {
  return (
    <Button onClick={onClick} className={`${className}`}>
      <span>{title}</span>
    </Button>
  );
}

export default ActionButton;
