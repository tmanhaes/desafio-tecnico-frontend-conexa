import styled from 'styled-components';

export const Button = styled.button`
  height: 40px;
  padding: 0 10px;
  border: none;
  border 2px solid var(--light-blue);
  border-radius: 6px;
  cursor: pointer;
  transition: all 350ms ease;
  
  &.helpButton{
    background: #fff;

    > span {
      color: var(--light-blue);
    }

    &:hover {
      background: var(--light-blue);
  
      > span {
        color: #fff;
      }
    }
  }
  
  &.scheduleButton {
    background: var(--light-blue);

    > span {
      color: #fff;;
    }
    
    &:hover {
      background: #fff;
  
      > span {
        color: var(--light-blue);
      }
    }
  }
`;
