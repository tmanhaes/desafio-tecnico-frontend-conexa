import { Login, NewConsultation } from '../types';
import { client } from './client';

const login = async (credentials: Login) => {
  if(!credentials){
    throw new Error('Necessário credenciais p/ login');
  }
  const response = (await client.post('/login', { email: credentials.email, password: credentials.password })).data;

  return response;
}

const getConsultations = async () => {
  const token = localStorage.getItem('token');
  const config = { 
    headers: { Authorization: `Bearer ${token}` } 
  };

  const consults = (await client.get(`/consultations?_expand=patient`, config)).data;
  return consults;
}

const getPatients = async () => {
  const token = localStorage.getItem('token');
  const config = { 
    headers: { Authorization: `Bearer ${token}` } 
  };

  const patients = (await client.get(`/patients`, config)).data;

  return patients;
}

const newConsultation = async (patientId: number, date: string) => {
  const token = localStorage.getItem('token');
  const config = { 
    headers: { Authorization: `Bearer ${token}` } 
  };
  
  const response = (await client.post('/consultations', { patientId, date: date }, config)).data;

  return response;
}

export {
  login,
  getConsultations,
  getPatients,
  newConsultation
}
