import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Login from './pages/Login';
import DoctorsRoom from './pages/DoctorsRoom';
import Page404 from './pages/Page404';

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/consultas" exact component={DoctorsRoom} />
        <Route component={Page404} />
      </Switch>
    </BrowserRouter>
  )
}

export default Routes;

