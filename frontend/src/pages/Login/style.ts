import styled from 'styled-components';

export const Container = styled.main`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  flex-wrap: rap;
  margin: 40px;
  height: 80vh;

  @media (max-width: 768px){
    flex-direction: column;
  }
`;
export const LeftContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 50%;

  @media (max-width: 768px){
    width: 100%;
  }
`;

export const RightContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 50%;

  @media (max-width: 768px){
    width: 100%;
  }
`;

export const TitleContainer = styled.div`
  > h1 {
    font-family: var(--font-Monserrat);
    font-size: var(--font-size-title);
    font-weight: var(--font-weight-bold);
    color: var(--dark-blue);
    margin: 30px 0 60px;
  }
`;

export const ImgContainer = styled.div`
  padding: 10px;

  @media (max-width: 768px){
    display: none;
  }
`;

export const LoginForm = styled.form`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  @media (max-width: 768px){
    width: 100%;
  }
`;

export const InputItem = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
  margin: 10px 0 20px;

  > label {
    font-family: var(--font-Nunito);
    font-weight: var(--font-weight-bold);
    color: var(--dark-gray);
  }

  > input {
    width: 250px;
    border: none;
    padding: 10px 4px;
    border-bottom: 2px solid var(--input-bottom-border);
    
    &::placeholder { 
      color: var(--light-gray);
    }

    @media (max-width: 768px){
      width: 100%;
    }
  }

  @media (max-width: 768px){
    width: 100%;
  }
`;

export const SendButton = styled.button`
  width: 250px;
  height: 40px;
  margin-top: 30px;
  cursor: pointer;
  border-color: transparent;
  background: var(--light-blue);
  border-radius: 8px;
  transition: all 350ms ease;

  &:hover {
    background: #fff;
    border-color: var(--light-blue);

    > span {
      color: var(--light-blue);
    }
  }

  > span {
    color: #fff;
    font-family: var(--font-Nunito);
    font-weight: var(--font-weight-bold);
  }

  @media (max-width: 768px){
    width: 100%;
  }
`;  
