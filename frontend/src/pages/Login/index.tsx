import React, { FormEvent, useState } from 'react';
import { 
  Container, 
  LeftContainer, 
  RightContainer, 
  TitleContainer, 
  ImgContainer, 
  LoginForm, 
  InputItem,
  SendButton
} from './style';
import Layout from '../../components/Layout';
import Header from '../../components/Header';
import HomeImage from '../../assets/home-left.svg';
import { login } from '../../services/index';
import { useHistory } from 'react-router-dom';
import { ILoginResponse } from '../../types';

const Login = () => {
  const history = useHistory();
  const [isLogged, setIsLogged] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const loginAction = async (event: FormEvent) => {
    event.preventDefault();

    const loginData = { email, password };

    try {
      const loginResponse: ILoginResponse = await login(loginData);
      if(loginResponse){
        localStorage.setItem('token', loginResponse.token)
        localStorage.setItem('name', loginResponse.name);
        localStorage.setItem('isLogged', 'true');
        history.push('/consultas');
      }
    }catch(e){
      alert(`não foi possível fazer login, tente novamente! erro:${e}`)
    }
  }

  return (
    <Layout>
      <Header isLogged={false}/>
      <Container >
        <LeftContainer>
          <TitleContainer>
            <h1>Faça Login</h1>
          </TitleContainer>
          <ImgContainer>
            <img src={HomeImage} alt="img"/>
          </ImgContainer>
        </LeftContainer>

        <RightContainer>
          <LoginForm onSubmit={loginAction}>
            <InputItem>
              <label>E-mail</label>
              <input 
                type="text" 
                required
                placeholder="Digite seu e-mail"
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </InputItem>
            <InputItem>
              <label>Senha </label>
              <input 
                type="password" 
                required
                placeholder="Digite sua senha"
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
            </InputItem>
            <SendButton type="submit">
              <span>Entrar</span>
            </SendButton>
          </LoginForm>
        </RightContainer>
      </Container>
    </Layout>
  )
}

export default Login;
