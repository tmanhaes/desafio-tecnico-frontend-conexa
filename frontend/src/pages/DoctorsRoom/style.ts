import styled from 'styled-components';

export const Container = styled.nav`
  min-height: calc(100% - 150px);
  padding: 0 20px;

  > strong {
    width: 100%;
    text-align: center;
  }
`;

export const Title = styled.h1`
  font-family: var(--font-Monserrat);
  font-size: var(--font-size-title);
  font-weight: var(--font-weight-bold);
  color: var(--dark-blue);
  margin: 30px 0 60px;
`;

export const Content = styled.main`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
  width: 550px;
  margin: 0 auto;

  @media(max-width: 768px){
    width: 90%;
  }
`;

export const ConsultsList = styled.ul`
  width: 550px;
  margin: 20px auto 0;
  margin: 20px auto 0;
  max-height: 300px;
  overflow-y: scroll;

  @media(max-width: 768px){
    width: 90%;
  }
`;

export const ConsultItem = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  margin: 15px 5px;

`;

export const Infos = styled.div`
  > h3 {
    font-family: var(--font-Nunito);
    font-weight: var(--font-weight-normal);
    font-size: var(--font-size-normal);
    color: var(--dark-gray);
    margin: 0;
  }

  > span {
    font-family: var(--font-Nunito);
    font-weight: var(--font-weight-normal);
    font-size: var(--font-size-small);
    color: var(--dark-gray);
  }
`;

export const Action = styled.div``;

export const NoConsult = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 300px;
  margin: 0 auto;

  @media(max-width: 768px){
    width: 200px;
  }
`;

export const ImageContainer = styled.div`
  margin: 15px 0;
  &.plant {
    align-self: flex-start;
  }
  
  &.certificates {
    align-self: flex-end;
  }
`;

export const TextContent = styled.div`
  margin: 15px 0;
  > strong {
    font-family: var(--font-Nunito);
    font-size: var(--font-size-medium);
    font-weight: var(--font-weight-bold);
    color: var(--light-gray);
  }
`;
