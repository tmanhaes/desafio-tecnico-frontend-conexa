import React, { useEffect, useState } from 'react';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Layout from '../../components/Layout';
import { 
  Container, 
  Title, 
  Content,
  ConsultsList, 
  ConsultItem ,
  Action, 
  Infos,
  NoConsult,
  ImageContainer,
  TextContent
} from './style';
import Plant from '../../assets/illustration-plant.svg';
import Certificate from '../../assets/illustration-certificates.svg';
import ActionButton from '../../components/ActionButton';
import { getConsultations, getPatients } from '../../services/index';
import { formatDate } from '../../helpers/format';
import { IConsultationsResponse, IPatient } from '../../types';
import { useHistory } from 'react-router-dom';

const ItemConsult: React.FC<IConsultationsResponse> = ({ id, patientId, date, patient }) => {
  const formattedDate = formatDate(date);

  const scheduleClick = () => {
    alert("Schedule click");
  }

  return(
    <ConsultItem>
      <Infos>
        <h3>{patient.first_name + " " + patient.last_name}</h3>
        <span>{formattedDate}</span>
      </Infos>
      <Action>
        <ActionButton 
          title="Atender" 
          className="scheduleButton" 
          onClick={scheduleClick}
        />
      </Action>
    </ConsultItem>
  )
}

const DoctorsRoom = () => {
  const history = useHistory();
  const [consultations, setConsultations] = useState<IConsultationsResponse[]>();
  const [qtdItems, setQtd] = useState(0);
  const [pacients, setAllPacients] = useState<IPatient[]>([]);

  useEffect(() => {
    (async () => {
      const response: IConsultationsResponse[] = await getConsultations();
      setConsultations(response);
      response.map((item, index) => setQtd(qtdItems + index));
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const response = await getPatients();
      setAllPacients(response);
    })()
  },[]);

  useEffect(() => {
    setConsultations([]);
  },[history])

  return (
    <Layout>
      <Header isLogged={true} />
      <Container>
        <Title>Consultas</Title>
        {
         consultations && consultations.length ? ( 
            <Content>
              <strong>{qtdItems+1} consultas agendadas</strong>
               <ConsultsList>
                 {
                   consultations.map((item, index) => {
                     return (
                       <ItemConsult
                          key={item.id}
                         id={item.id}
                         patientId={item.patientId}
                         patient={item.patient}
                         date={item.date}
                       />
                     )
                   })
                 }
               </ConsultsList>
            </Content>
         ): ( 
          <NoConsult>
            <ImageContainer className="certificates">
              <img src={Certificate} alt="certificate" />
            </ImageContainer>

            <TextContent>
              <strong>Não há nenhuma consulta agendada</strong>
            </TextContent>

            <ImageContainer className="plant">
              <img src={Plant} alt="plant" />
            </ImageContainer>
          </NoConsult>
           
         )
        }
      </Container>
      <Footer pacients={pacients ? pacients : []}/>
    </Layout>
  );
}

export default DoctorsRoom;
