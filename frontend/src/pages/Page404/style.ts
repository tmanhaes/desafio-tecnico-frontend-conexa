import styled from 'styled-components';

export const Container = styled.main`
  min-height: 85vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  > h1 {
    font-family: var(--font-Monserrat);
    font-size: var(--font-size-title);
    font-weight: var(--font-weight-bold);
    color: var(--dark-blue);
  }

  > a {
    font-family: var(--font-Monserrat);
    font-size: var(--font-size-large);
    font-weight: var(--font-weight-bold);
    color: var(--dark-blue);
  }
`;
