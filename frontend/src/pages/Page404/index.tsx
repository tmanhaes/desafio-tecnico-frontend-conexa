import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from './style';

const Page404 = () => {
  const isLogged = localStorage.getItem('isLogged');

  return (
    <Container>
      <h1>Página não encontrada</h1>
      <Link to={`${isLogged === 'isLogged' ? '/consultas' : '/'}`}>Home</Link>
    </Container>
  )
}

export default Page404;
